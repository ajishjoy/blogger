<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //
    protected $fillable = [
        'user_id', 'title', 'topic', 'description',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        // your other new column
    ];

    public function User()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
