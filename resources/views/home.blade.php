@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md">
			<div class="card">
				<div class="card-header">Dashboard</div>
					<div class="card-body">
						@if (session('status'))
							<div class="alert alert-success" role="alert">
								{{ session('status') }}
							</div>
						@endif
						<div class="row row-cols-1 row-cols-md-3">
							<div class="col mb-4">
								<div class="card">
									<div class="card-body">
										<h5 class="card-title">Create Post</h5>
										<p class="card-text">Create a Post</p>
									<a class="btn btn-primary " href="{{ url('post/create') }}" role="button" style="width: 100%">Create</a>
									</div>
								</div>
							</div>
							<div class="col mb-4">
								<div class="card">
									<div class="card-body">
										<h5 class="card-title">Show, Edit and delete Post</h5>
										<p class="card-text">Show, Edit and delete your post </p>
										<a class="btn btn-primary " href="{{ route('post.show', Auth::id()) }}" role="button" style="width: 100%">Show Post</a>
									</div>
								</div>
							</div>	
						</div>
					</div>
				</div>
			</div>
    	</div>
	</div>
</div>
@endsection
