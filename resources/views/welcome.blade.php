@extends('layouts.app')

        @section('content')
            <div class="container">
                
            <div class="row justify-content-center">
                <div class="col-md">
                    <div class="card">
                        <div class="card-header">Our Users Posts</div>
                            <div class="card-body">
                            <form action="{{ url('/filter') }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="title">Select topic you wish to Filter</label>
                                        <select class="form-control"  name="topic" required autofocus> 
                                            <label for="exampleInputEmail1">Title</label>
                                            <option>Information Technology</option>
                                            <option>Finance</option>
                                            <option>Sports</option>
                                            <option>Stock Market</option>
                                            <option>Covid - 19</option>
                                            <option>Banking Industry</option>
                                            <option>Social Media</option>
                                            <option>Mobile Phones</option>
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </form>
                            </div>
                            <div class="card-body">
                                @foreach ($posts as $post)
                                    <div class="row row-cols-1 row-cols-md-3">
                                    <div class="col mb-4">
                                        <div class="card">
                                            <div class="card-body">
                                                <h5 class="card-title">{{ $post->title }}</h5>
                                                <p class="card-text"><small class="text-muted">Created {{ \Carbon\Carbon::parse($post->created_at)->diffForHumans() }}</small></p>
                                                <p class="card-text">{{ $post->topic }}</p>
                                                <p class="card-text">{{ $post->description }}</p>
                                                <p class="card-text"><small class="text-muted">Updated {{ \Carbon\Carbon::parse($post->updated_at)->diffForHumans() }}</small></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                {{ $posts->links() }}
                            </div>
                        </div>
                    </div>
                </div>    
            </div>            
        @endsection
