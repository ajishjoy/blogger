@extends('layouts.app')

@section('content')
  <div class="container">
    @if(session()->has('message'))
      <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <i class="zmdi zmdi-close"></i>
        </button>
        <strong>
            <i class="zmdi zmdi-check"></i> Success!</strong> {{ session()->get('message') }}
      </div>
    @endif
    <form action="{{ route('post.update', $post->id) }}" method="post">

      {{ csrf_field() }}

      <input type="hidden" name="_method" value="PUT">
      <input type="hidden" value="{{ Auth::id() }}" class="form-control" id="user_id" name="user_id" >
      <div class="form-group">
        <label for="title">Title</label>
      <input type="text" class="form-control" id="title" placeholder="Enter title" value="{{ $post->title, $post->id }}" name="title" autocomplete="off" required>
      </div>
      <div class="form-group">
        <label for="title">Select Topic</label>
        <select class="form-control"  name="topic" required> 
          <label for="exampleInputEmail1">Title</label>
          <option>Information Technology</option>
          <option>Finance</option>
          <option>Sports</option>
          <option>Stock Market</option>
          <option>Covid - 19</option>
          <option>Banking Industry</option>
          <option>Social Media</option>
          <option>Mobile Phones</option>
          <option>Ministry of External Affairs</option>
        </select>
      </div>
      <div class="form-group">
        <label for="exampleFormControlTextarea1">Description</label>
      <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="description" required>{{ $post->description }}</textarea>
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
      <a class="btn btn-primary " href="{{ url('/post') }}" role="button" >Go back</a>
    </form>
  </div>
@endsection