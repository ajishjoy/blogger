@extends('layouts.app')

@section('content')

<div class="container">
<div class="row justify-content-center">
    <div class="col-md">
        <div class="card">
            <div class="card-header">Your Posts <a class="btn btn-primary" href="{{ url('/home') }}" role="button" >Go back</a></div>
                <div class="card-body">
                    @if(session()->has('message'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="zmdi zmdi-close"></i>
                            </button>
                            <strong>
                                <i class="zmdi zmdi-check"></i> Success!</strong> {{ session()->get('message') }}
                        </div>
                        @endif
                    
                    @foreach ($posts as $post)
                        <div class="row row-cols-1 row-cols-md-3">
                        <div class="col mb-4">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $post->title }}</h5>
                                    <p class="card-text"><small class="text-muted">Created {{ \Carbon\Carbon::parse($post->created_at)->diffForHumans() }}</small></p>
                                    <p class="card-text">{{ $post->topic }}</p>
                                    <p class="card-text">{{ $post->description }}</p>
                                    <p class="card-text"><small class="text-muted">Updated {{ \Carbon\Carbon::parse($post->updated_at)->diffForHumans() }}</small></p>
                                    <form method="post" action="{{ route('post.destroy', $post->id) }}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <a class="btn btn-primary " href="{{ route('post.edit', $post->id) }}" role="button" >Edit</a>
                                        <input class="btn btn-danger" type="submit" value="DELETE" ;>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    
                </div>
            </div>
        </div>
    </div>    
</div>


@endsection